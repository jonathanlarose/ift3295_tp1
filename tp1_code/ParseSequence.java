import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ParseSequence {
	
	public static String [] parse (String pathFichier){
		String [] sequences = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(pathFichier));
	        String line = "";
	        String line2 = "";

	        	try {
	                int nbLignes = 0;

	                while ((line = br.readLine()) != null){

	                    if (line.contains("@") || line.contains("+") || line.contains("2") || line.contains(">"))
	                        continue;

	                    nbLignes++;
	                }

	                br.close();

	                BufferedReader br2 = new BufferedReader(new FileReader(pathFichier));
	                sequences = new String[nbLignes];
	                int compteur = 0;

	                while (compteur < nbLignes) {

	                    if ((line2 = br2.readLine()) != null && compteur < nbLignes)
	                        if (line2.contains("@") || line2.contains("+") || line2.contains("2"))
	                            continue;

	                    sequences[compteur] = line2;
	                    compteur++;
	                }

	                br2.close();
	            } catch (IOException e) {
	                System.out.println(e.toString());
	            }    

	        } catch (FileNotFoundException ex) {
	            System.out.println("le fichier n'existe pas");
	        }
		 return sequences;
	}
	
	public static String [] parseAcidesAmines (String pathFichier){
		String [] sequencesAA = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(pathFichier));
			
	        String line2 = "";

	        	try {
	                int nbLignes = 0;

	                while ((br.readLine()) != null){      

	                    nbLignes++;
	                }

	                br.close();

	                BufferedReader br2 = new BufferedReader(new FileReader(pathFichier));
	                
	                sequencesAA = new String[nbLignes];
	                int compteur = 0;

	                while (compteur < nbLignes) {
	                	
	                	line2 = br2.readLine();
	                    sequencesAA[compteur] = line2;
	                    compteur++;
	                }

	                br2.close();
	            } catch (IOException e) {
	                System.out.println(e.toString());
	            }    

	        } catch (FileNotFoundException ex) {
	            System.out.println("le fichier n'existe pas");
	        }
		 return sequencesAA;
	}
}
