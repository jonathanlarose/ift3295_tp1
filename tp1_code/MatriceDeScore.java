/**
 * Created by dennisorozco on 17-09-24.
 */

public class MatriceDeScore {

    private int longueur;
    private int[][] matriceDeScores;
    private int[][] matriceAdj;
    private int[][] matriceAdjReduite;
    private Alignement[][] mAlignement;
    private String[] sequences;

    public int[][] getMScores(){return matriceDeScores;}
    public Alignement[][] getMAlignement(){return mAlignement;}

    public MatriceDeScore(String[] sequences, int min){

        int l = sequences.length;

        this.matriceDeScores = new int[l][l];
        this.matriceAdj = new int[l][l];
        this.matriceAdjReduite = new int[l][l];
        this.mAlignement = new Alignement[l][l];
        this.sequences = sequences;

        calculMatrice();

        //Initialiser les matrices d'adjacences sans les scores sous le seuil min
        for(int i=0; i < matriceAdj.length; i++) {
            for (int j = 0; j < matriceAdj.length; j++) {
                matriceAdj[i][j] = (matriceDeScores[i][j] >= min) ? matriceDeScores[i][j] : 0;
                matriceAdjReduite[i][j] = matriceAdj[i][j];
            }
        }

        reductionTransitive();
    }

    public int [][] calculMatrice(){

        longueur = sequences.length;

        for (int i = 0; i < longueur; i++){

            for (int j = i+1; j < longueur; j++) {

                if(i==j)
                    continue;

                mAlignement[i][j] = new Alignement(sequences[i], sequences[j], 4, -4, -8);
                mAlignement[j][i] = mAlignement[i][j];
                Alignement temp = mAlignement[i][j];

                if (temp.getAlignedSeq1().startsWith("-"))
                    matriceDeScores[j][i] = temp.getScore();
                else
                    matriceDeScores[i][j] = temp.getScore();
            }
        }
        return this.matriceDeScores;
    }

    private void reductionTransitive(){

        int max = 0;
        int tmp;
        int jMax = -1;

        //Reduction
        for(int i=0; i<matriceAdjReduite.length; i++){
            for(int j=0; j<matriceAdjReduite.length; j++){

                tmp = matriceAdjReduite[i][j];

                if(tmp > max){
                    if(jMax >= 0)
                        matriceAdjReduite[i][jMax] = 0;

                    jMax = j;
                    max = tmp;
                    matriceAdjReduite[i][j] = max;
                }
                else
                    matriceAdjReduite[i][j] = 0;
            }
            max = 0;
            jMax = -1;
        }

        return;
    }

    //Fonction qui trouve le premier segment de la sequence et initialise l'attribut premierSegment
    private int trouverPremierSegment(){

        int sum = 0;

        //On trouve la colonne
        for(int j=0; j<matriceAdjReduite.length; j++) {
            for (int i = 0; i < matriceAdjReduite.length; i++) {

                sum += matriceAdjReduite[i][j];
            }

            if(sum == 0){
                return j;
            }
            sum = 0;
        }

        return -1;
    }

    //Fonction qui retourne la sequence des construit avec les fragments de sequences
    public String buildSequence(){

        int i = trouverPremierSegment();
        String seq = "";
        boolean done = false;
        Alignement aTmp;
        String seqP;
        String seqS;
        int endSeq;

        while(!done){

            for(int j=0; j < matriceAdjReduite.length; j++){

                if(matriceAdjReduite[i][j] != 0){

                    aTmp = mAlignement[i][j];

                    if(aTmp.getAlignedSeq1().startsWith("-")){
                        seqP = aTmp.getAlignedSeq2();
                        seqS = aTmp.getAlignedSeq1();
                    }
                    else{
                        seqP = aTmp.getAlignedSeq1();
                        seqS = aTmp.getAlignedSeq2();
                    }

                    endSeq = 0;

                    while(seqS.charAt(0) == '-'){
                        endSeq++;
                        seqS = seqS.substring(1);
                    }

                    if(seq.length() == 0)
                        seq = seqP.substring(0,endSeq + aTmp.getChevauchement());

                    seq += seqS.substring(aTmp.getChevauchement());

                    System.out.println("(" + (i+1) + ", " + (j+1) + ") :  " + aTmp.getChevauchement());
                    //System.out.println(aTmp);

                    i = j;

                    done = false;
                    break;
                }
                else
                    done = true;
            }
        }

        return seq;
    }

    public String toString (){

        String resultat = "La matrice de matriceDeScores est : \n\n";

        String bordure = "";

        for(int i=0; i<20; i++)
            bordure += "|----";

        bordure += "|";

        resultat += bordure + "\n";

        for (int i = 0; i < longueur; i++){

            resultat += "|";

            for (int j = 0; j < longueur; j++){

                resultat += matriceDeScores[i][j];

                if (matriceDeScores[i][j] <= 9)
                    resultat += "   ";

                else if (matriceDeScores[i][j] >= 10 && matriceDeScores[i][j] <= 99)
                    resultat += "  ";

                else if (matriceDeScores[i][j] >= 100 && matriceDeScores[i][j] <= 999)
                    resultat += " ";

                resultat += "|";
            }

            resultat += "\n" + bordure + "\n";
        }
        return resultat;
    }


    public String edgeList(){

        return matriceToEdgeList(this.matriceAdj);
    }

    public String edgeListReduce(){

        return matriceToEdgeList(this.matriceAdjReduite);
    }


    private String matriceToEdgeList(int[][] m) {

        String list = "";

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m.length; j++) {

                if (i == j)
                    continue;

                if (m[i][j] != 0)
                    list += "seq" + (i+1) + "\t" + "seq" + (j+1) + "\t" + m[i][j] + "\n";
            }
        }

        return list;
    }
}
