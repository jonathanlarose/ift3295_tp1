/**
 * Created by dennisorozco on 17-09-27.
 */

public class Main {
    public static void main (String [] args){

        //Parsing du fichier de reads
    	String pathFichier = "reads.fq";
        String [] sequences = ParseSequence.parse(pathFichier);

        //Matrice de score des différents alignement
        MatriceDeScore analyse = new MatriceDeScore(sequences,80);

        System.out.println(analyse);

        //Graphe
        String edgeList = analyse.edgeList();
        String edgeListReduce = analyse.edgeListReduce();
        
        EcrireFichierTxt.write(edgeList,"graphe.txt");
        EcrireFichierTxt.write(edgeListReduce,"grapheReduce.txt");

        //Les brins reverse sont : 6,7,9,10,12,15,18
        int[] reverseIndex = {6,7,9,10,12,15,18};

        String[] sequencesCorrige = new String[sequences.length];
        System.arraycopy(sequences, 0, sequencesCorrige, 0, sequences.length);

        for(int i=0; i<reverseIndex.length; i++)
            sequencesCorrige[reverseIndex[i]-1] = SequenceADN.complement(sequencesCorrige[reverseIndex[i]-1]);

        //Nouvelle Matrice des scores d'alignement avec les bon brins fowards
        MatriceDeScore brinsFoward = new MatriceDeScore(sequencesCorrige, 80);
        //Graph graph2 = new Graph(brinsFoward, 80);

        String eFull = brinsFoward.edgeList();
        String eReduce = brinsFoward.edgeListReduce();

        EcrireFichierTxt.write(eFull, "grapheFoward.txt");
        EcrireFichierTxt.write(eReduce, "grapheFowardReduce.txt");


        String sequenceFinal = brinsFoward.buildSequence();
        System.out.println("Sequence Final :\n\n" + sequenceFinal);
        System.out.println("longueur = " + sequenceFinal.length());

        String sequenceFourni = "AGGACAAAGTTTGCCTTACCATATGTTTCCTGATCGTGCGAAAGCCCTTTTCTAGGGACTTGGAGAACACAAGTATTATGAAAAGTACTGATGAAAGTTATTAACAGGTTTCGAAAAATAACTTTACTATCTGACGTGTTGCTTCTGCCGAGGATGACCGTTATTCCTGTTTTTGCATGTATATTTCACGTATGGTTAAATGTGCCAGCGTTGTGGTTTAAAACTAATAGTAATAATATGCTTCTTTGTTCAGTTGGCTAGAGATTTACTACATCCGTCCTTGGAAGAGGAAAAGAAAAAACATAAAAAGAAACGCCTAGTACAAAGTCCAAATTCTTACTTTATGGATGTAAAATGTCCAGGTAAAATTTGAAATCTTAATTCCTTTACTAAAGAAAATTTCTGTAGGGATTGCTAGTGTGGTGTGTATAGTTAAGATACATTAGAACCCTCTGTTGAGTAGAAGTGGGATTACAGAATTGGAAATGTCAGGGACATTTTCATAATAAATGTACTGAAACCATTGTAGAAACATTTCGGGGTAAGTGAAAACAGGCATTCAAAGGAGATATGCCATTTGACTGCACTTGTGGATAATCAAAATGTGGATACTCAAAATAGAGATGTTCTTTAATTGTAAAATACTCACTGGATTTTGACGATGTAGCACAGAAAAAAAATACATTGATTACACTGTTTTTAAAAAATTTTGTGTCGCTGCTAGAAAAGTTTATGTTACACATGTGGCTTGCTGTTTCATAGCACTGAAGTTACTGATTTTTTTACATATTACCAGAAATGTCGAACAGGTGCTGTTTTCCTCTGCTTTTCATTTTTAACATTGCCTTTTTTTTTTTTTAGTTTGCTACAAGATCACCACGGTTTTCAGCCATGCTCAGACAGTGGTTCTTTGTGTAGGTTGTTCAACAGTGTTGTGCCAGCCTACAGGAGGAAAGGCCAGACTCACAGAAGGATTATCATTTGGCATTCTCCAACCCAGTGATGAGATTGATGATTATAAATGTCTCTATCTTCACTGAAAAGTTTAAAGAAATCTTAATGATTACCAAAATAACTTATCTCTCACTGGAAGAGTTCAAGTGGATTGGCAGCAAATCTGAGATCTATTTGGTGTGACCTGGTGAGATCTAAATATGGAGTCAGCACATGATTTTTTTAAGAGTAATATTGCTAAGTAATATTGCTAAGTATAGTCTGAAAATACCTCTAATCAAAATTATTTACTTGAGAAAAGTATTCAGAATAGTTCCTAAAAATTAAGAGTATATTTCTGGTATAAAAGGATAAATAATCTGTATATGAGTATTAATCCAATATTCTTAAAACTTCAGTATTTTACTTAAAAGTACTTTTTGTCATTAAAATTATAGCAAAGGTAGAATGCACTTGTTTAATATACTCTCATGATTCTTTTGCAGATTGTTCATTTAGAAGAAAGCAACACTAATGATTCAAACAGCTTCCTGAATTTTAATTTTGTGTTGTCTCACAGAAAGCCTTATCATAAATTCCATAATTCTAATTAATTTACCAAGATAATGTAATTACATTTGGTTTTGTAAGGTATACAGCAGTAATCTCCTATTTTGGTGTCAGTTTTTCAATAAAGTTTTGATTATGGGCAAA";
        System.out.println(sequenceFourni.length());

        System.out.println(new Alignement(sequenceFinal, sequenceFourni, 4, -4, -8));
        
        // parsing des fichiers
        String pathFichiergeneX = "geneX.fasta";
        String pathFichierFasta = "sequence.fasta";
        String pathFichierAcidesAmines = "AcidesAmines.txt";

        String [] sequenceFasta = ParseSequence.parse(pathFichierFasta);
        String [] sequenceGeneX = ParseSequence.parse(pathFichiergeneX);

        String geneX = "MCQRCGLKLIVIICFFVQLARDLLHPSLEEEKKKHKKKRLVQSPNSYFMDVKCPGCYKITTVFSHAQTVVLCVGCSTVLCQPTGGKARLTEGCSFRRKQH";

        SequenceProt fasta = new SequenceProt(sequenceFasta[0], pathFichierAcidesAmines);

        String arnm = fasta.trouverARNm(geneX);
        System.out.println(arnm);

        SequenceProt traductionArnm = new SequenceProt(arnm, pathFichierAcidesAmines);

        System.out.println(traductionArnm.getCadreLecture()[0]);

        System.out.println("Done!");
    }
}