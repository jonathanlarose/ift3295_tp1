/**
 * Created by dennisorozco on 17-09-27.
 */

public class Main {
    public static void main (String [] args){

        //Parsing du fichier de reads
        String [] sequences = ParseSequence.parse(args[0]);

        Alignement a = new Alignement(sequences[0], sequences[1], 4, -4, -8);

        System.out.println(a);
    }
}