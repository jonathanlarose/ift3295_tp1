/**
 * Created by Jonathan on 2017-09-28.
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

public class EcrireFichierTxt {

    public static void write(String content, String fileName) {

        File fichier = new File(fileName);
        FileWriter fw = null;
        BufferedWriter bw = null;

        //Écrit le fichier contenant le graphe
        try {

            fw = new FileWriter(fichier);
            bw = new BufferedWriter(fw);
            bw.write(content);

        } catch (IOException e) {

            System.out.println("Problème à l'écriture du fichier");
            return;

        } finally {

            try {
                bw.flush();
                bw.close();
                fw.close();

            } catch (Exception e) {

            }

        }
        System.out.println("Fichier enregistrer.");
    }
}
