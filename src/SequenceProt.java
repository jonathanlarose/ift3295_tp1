/**
 * Created by Jonathan on 2017-10-01.
 */
public class SequenceProt {

    private String adn;
    private String[] cadreLecture;

    public String[] getCadreLecture() {
        return cadreLecture;
    }

    public SequenceProt(String adn, String pathAcidesAmines){

        this.adn = adn;
        this.cadreLecture = traduitSequence(adn, pathAcidesAmines);
    }

    private String [] traduitSequence(String sequence, String pathAcidesAmines){

        String [] acidesAminesTab = ParseSequence.parseAcidesAmines(pathAcidesAmines);
        String[] cadreLecture = new String [3];

        cadreLecture[0] = match(sequence, acidesAminesTab);;
        cadreLecture[1] = match(sequence.substring(1), acidesAminesTab);
        cadreLecture[2] = match(sequence.substring(2), acidesAminesTab);

        return cadreLecture;
    }


    public static String match(String sequence, String[] acidesAminesTab){
        String listeAA = "";

        for (int i= 0; i < sequence.length() - 2; i++){
            String compare = sequence.substring(i, i+3);
            i += 2;

            for(int j = 0; j < acidesAminesTab.length; j ++){
                if(acidesAminesTab[j].contains(compare))
                    listeAA += acidesAminesTab[j].charAt(0);
            }
        }
        return listeAA;
    }

    public String trouverARNm(String gene){

        String arnm = "";
        String exon;
        String prot = gene;

        while(!prot.equals("")){

            exon = trouverExon(prot);
            arnm += exon;

            prot = prot.substring(exon.length()/3);
        }

        return arnm;
    }

    private String trouverExon(String prot){

        String exon = "";
        String frag = "";
        int index = -1;

        for(int i=1; i <= prot.length(); i++){

            frag = prot.substring(0,i);

            if(!(cadreLecture[0].contains(frag) || cadreLecture[1].contains(frag) || cadreLecture[2].contains(frag))) {
                frag = frag.substring(0,frag.length()-1);
                break;
            }
        }

        if(cadreLecture[0].contains(frag)){
            index = cadreLecture[0].indexOf(frag)*3;
        }
        else if(cadreLecture[1].contains(frag)){
            index = cadreLecture[1].indexOf(frag)*3 + 1;
        }
        else if(cadreLecture[2].contains(frag)){
            index = cadreLecture[2].indexOf(frag)*3 + 2;
        }

        exon = adn.substring(index, index + frag.length()*3);

        System.out.println(index + " à " + (index + frag.length()*3));
        System.out.println(exon);

        return exon;
    }
}
