/**
 * Created by Jonathan on 2017-09-29.
 */
public class SequenceADN {

    public static String complement(String original){

        String comp = "";

        while(original.length() > 0){

            comp = nuclComp(original.charAt(0)) + comp;
            original = original.substring(1);
        }

        return comp;
    }


    private static char nuclComp(char o){

        char c;

        switch (o){
            case 'A':
                c = 'T';
                break;
            case 'C':
                c = 'G';
                break;
            case 'T':
                c = 'A';
                break;
            case 'G':
                c = 'C';
                break;
            default:
                c = 'Z'; //Pour repérer les erreurs
        }
        return c;
    }
}
