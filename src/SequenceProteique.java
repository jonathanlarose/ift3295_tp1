
public class SequenceProteique {
	
	public static String [] traduitSequence(String sequence, String pathAcidesAmines){
		
		String [] acidesAminesTab = ParseSequence.parseAcidesAmines(pathAcidesAmines);
		String[] cadreLecture = new String [3];
		String sequence1 = "";
		String sequence2 ="";
		String sequence3 = "";
		
		sequence1 += match(sequence, acidesAminesTab);
		sequence2 += match(sequence.substring(1), acidesAminesTab);
		sequence3 += match(sequence.substring(2), acidesAminesTab);
		System.out.println(sequence1);
		System.out.println(sequence2);
		System.out.println(sequence3);
		
		cadreLecture[0] = sequence1;
		cadreLecture[1] = sequence2;
		cadreLecture[2] = sequence3;
		
		return cadreLecture;
	}
	
	public static String match(String sequence, String[] acidesAminesTab){
		String listeAA = "";
		
		for (int i= 0; i < sequence.length() - 2; i++){
			String compare = sequence.substring(i, i+3);
			i += 2;
			
			for(int j = 0; j < acidesAminesTab.length; j ++){
				if(acidesAminesTab[j].contains(compare))
					listeAA += acidesAminesTab[j].charAt(0);
			}
		}
		return listeAA;
	}
	
	// fonction qui retourne les intervalles contenant la sequence de la proteine X
	
	public static void intervalles(String sequence1, String sequence2, String sequenceProteine){
		
		int row = sequenceProteine.length() + 1;
		int col1 = sequence1.length();
		int col2 = sequence2.length();
		
		// les coordonnes sont l'indice du tableau et son contenu
		
		int [] intervalles1 = new int [sequenceProteine.length() + 1];
		int [][] scores1 = new int [row][col1];
		
		int [] intervalles2 = new int [sequenceProteine.length() + 1];
		int [][] scores2 = new int [row][col2];
		
		// Remplir la table contenant les fragments de sequence de la proteine
		// Retrouver le score maximal d'alignement dans chaque ligne
		
		int maxLigne;
		for (int i = 1; i < row; i++){
			maxLigne = 0;
			for (int j = 1; j < col1; j++){
			
				if (sequenceProteine.charAt(i-1) == sequence1.charAt(j-1)){
					scores1[i][j] = scores1[i-1][j-1] + 1;
					
					if (scores1[i][j] >= maxLigne){
						maxLigne = scores1[i][j];
						intervalles1[i] = j; 
					}
				} 	
			}
		}
		for (int i = 1; i < row; i++){
			maxLigne = 0;
			for (int j = 1; j < col2; j++){
			
				if (sequenceProteine.charAt(i-1) == sequence2.charAt(j-1)){
					scores2[i][j] = scores2[i-1][j-1] + 1;
					
					if (scores2[i][j] >= maxLigne){
						maxLigne = scores2[i][j];
						intervalles2[i] = j; 
					}
				} 	
			}
		}
		String arn = getArn(intervalles1, sequence1, scores1);
		String arn2 = getArn(intervalles2, sequence2, scores2);
		System.out.println("Sequence1 obtenue apres epissage1 : "+arn);
		System.out.println("Sequence2 obtenue apres epissage2 : "+arn2);
		System.out.println(arn);
	}
	
	static String getArn(int [] intervalles, String sequence, int[][] scores){
		
		String arn = "";
		int scoreMaxLigne;
		
		for (int i = intervalles.length - 1; i > 0; i--){
			int indice = intervalles[i]; 
			scoreMaxLigne = scores[i][indice];	// indice avec le plus grand score de la derniere ligne
			
			for (int j = 0; j < scoreMaxLigne; j++){
				arn += sequence.charAt((indice - 1) - j);
				i--;
			}	
		}

		//System.out.println(sequence);
		arn = new StringBuilder(arn).reverse().toString();
		return arn;		
	}
}
